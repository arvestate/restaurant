<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{homefeatured}prestashop>homefeatured_cd2c2dc24eaf11ba856da4a2e2a248be'] = 'Spesifikasi Produk di homepage';
$_MODULE['<{homefeatured}prestashop>homefeatured_b494beb5de5dd7b4fad67262e0de6321'] = 'Tampilkan Spesifikasi Produk di tengah halaman';
$_MODULE['<{homefeatured}prestashop>homefeatured_ced2b9f1aad1f8a5b351a3120e4b1212'] = 'Jumlah produk salah';
$_MODULE['<{homefeatured}prestashop>homefeatured_c888438d14855d7d96a2724ee9c306bd'] = 'Pengaturan terupdate';
$_MODULE['<{homefeatured}prestashop>homefeatured_f4f70727dc34561dfde1a3c529b6205c'] = 'Pengaturan';
$_MODULE['<{homefeatured}prestashop>homefeatured_20c47700083df21a87e47d9299ef4dc4'] = 'Jumlah produk yang ditampilkan';
$_MODULE['<{homefeatured}prestashop>homefeatured_ada7594fb1614c6b6d271699c8511d5e'] = 'Jumlah produk yang ditampilkan di homepage (default:10)';
$_MODULE['<{homefeatured}prestashop>homefeatured_c9cc8cce247e49bae79f15173ce97354'] = 'Simpan';
$_MODULE['<{homefeatured}prestashop>homefeatured_4351cfebe4b61d8aa5efa1d020710005'] = 'Lihat';
$_MODULE['<{homefeatured}prestashop>homefeatured_2d0f6b8300be19cf35e89e66f0677f95'] = 'Tambahkan ke troli';
$_MODULE['<{homefeatured}prestashop>homefeatured_e0e572ae0d8489f8bf969e93d469e89c'] = 'Tidak ada Spesifikasi Produk';
