<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockrss}prestashop>blockrss_2516c13a12d3dbaf4efa88d9fce2e7da'] = 'blok feed RSS';
$_MODULE['<{blockrss}prestashop>blockrss_be3b59f9eca89af2df840316a25e4b1d'] = 'Tambahkan blok untuk menampilkan feed RSS';
$_MODULE['<{blockrss}prestashop>blockrss_9680162225162baf2a085dfdc2814deb'] = 'feed RSS';
$_MODULE['<{blockrss}prestashop>blockrss_6706b6d8ba45cc4f0eda0506ba1dc3c8'] = 'feed URL tdk valid';
$_MODULE['<{blockrss}prestashop>blockrss_36ed65ce17306e812fd68d9f634c0c57'] = 'Judul tdk valid';
$_MODULE['<{blockrss}prestashop>blockrss_1b3d34e25aef32a3c8daddfff856577f'] = 'jumlah feed tdk valid';
$_MODULE['<{blockrss}prestashop>blockrss_c888438d14855d7d96a2724ee9c306bd'] = 'Pengaturan terupdate';
$_MODULE['<{blockrss}prestashop>blockrss_f4f70727dc34561dfde1a3c529b6205c'] = 'Pengaturan';
$_MODULE['<{blockrss}prestashop>blockrss_b22c8f9ad7db023c548c3b8e846cb169'] = 'Judul blok';
$_MODULE['<{blockrss}prestashop>blockrss_2343a40bdffd8c7a6317b6d98c2b1042'] = 'Buat judul blok (default:\'feed RSS\')';
$_MODULE['<{blockrss}prestashop>blockrss_402d00ca8e4f0fff26fc24ee9ab8e82b'] = 'Tambah feed URL';
$_MODULE['<{blockrss}prestashop>blockrss_bf711b813a11f4dd1b6fbde4a439b11a'] = 'Tambahkan feed URL yg tidak anda inginkan';
$_MODULE['<{blockrss}prestashop>blockrss_ff9aa540e20285875ac8b190a3cb7ccf'] = 'Jumlah urutan yg akan tampil';
$_MODULE['<{blockrss}prestashop>blockrss_595f3fab84b35a00db28d3eb3b3d92fa'] = 'Jumlah urutan yg akan tampil di blok (nilai default:5)';
$_MODULE['<{blockrss}prestashop>blockrss_c9cc8cce247e49bae79f15173ce97354'] = 'Simpan';
$_MODULE['<{blockrss}prestashop>blockrss_10fd25dcd3353c0ba3731d4a23657f2e'] = 'Tdk ada feed RSS ditambahkan';
