<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'blok Pelanggan';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_4ccf8ae00d5eef4ad158575ecf2fdb7d'] = 'Tambahkan blok Pelanggan';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Selamat Datang';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Keluarkan saya';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_4394c8d8e63c470de62ced3ae85de5ae'] = 'Keluar';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_bffe9a3c9a7e00ba00a11749e022d911'] = 'Masuk';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_12641546686fe11aaeb3b3c43a18c1b3'] = 'Troli Anda';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_7fc68677a16caa0f02826182468617e6'] = 'Troli:';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_f5bf48aa40cad7891eb709fcf1fde128'] = 'produk';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_86024cad1e83101d97359d7351051156'] = 'produk';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(kosong)';
$_MODULE['<{blockuserinfo}prestashop>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Akun anda';
