<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_6232eaff79c9ccb6c1a66e5a75a212d5'] = 'Blok Penjualan Terlaris';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_56710978dfca4fcdf98977fcaa4a553f'] = 'Tambahkan blok untuk tampilkan Penjualan terlaris';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_3cb29f0ccc5fd220a97df89dafe46290'] = 'Terlaris';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_eae99cd6a931f3553123420b16383812'] = 'Semua yang terlaris';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_f7be84d6809317a6eb0ff3823a936800'] = 'Saat ini tidak ada yang terlaris';
