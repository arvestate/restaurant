<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_2074615eb70699e55b1f9289c6c77c25'] = 'Modul kirim ke teman';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_b6835a400c888d666e6e5aee9f2a7bc6'] = 'Membolehkan pelanggan untuk mengirim link produk ke teman';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_c38f24359344dada5d54e7eef08de8bb'] = 'Semua field harus diisi';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_47f11ea69ec3161cab7c3434290efc8a'] = 'Email teman anda salah.';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_97d4e28297c641ed0dbe3f0172f17fa6'] = 'Nama teman anda salah.';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_22c4733a9ceb239bf825a0cecd1cfaec'] = 'Teman';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_2107f6398c37b4b9ee1e1b5afb5d3b2a'] = 'Kirim ke teman';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_20589174124c25654cac3736e737d2a3'] = 'Kirim halaman ini ke teman yang mungkin tertarik pada item berikut.';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_cc5fd9b9f1cad59fcff97a1f21f34304'] = 'Kirim pesan';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_19d305aea0ccec77d23362111ebdb6b4'] = 'Namanya Teman';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_7ae8a9a7a5d8fa40d4515fc52f16bb2e'] = 'Emailnya Teman';
$_MODULE['<{sendtoafriend}prestashop>sendtoafriend_2541d938b0a58946090d7abdde0d3890'] = 'Kirim';
