<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_9e31b08a00c1ed653bcaa517dee84714'] = 'blok Newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e95e612048e381ba2b1f310e07d1b1a3'] = 'Tambahkan blok untuk berlangganan newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_3f9b87832191fff1d2136c2c2d699e76'] = 'Pengupdatean berhasil';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f4f70727dc34561dfde1a3c529b6205c'] = 'Pengaturan';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_948f6c829388992fb60b74b55d4c9cc4'] = 'Tampilkan konfigurasi pada halaman baru?';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_a6105c0a611b41b08f1209506350279e'] = 'ya';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_7fa3b767c460b54a2be4d49030b349c7'] = 'tdk';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_e6c433ce875a8034672996e8aff25fc2'] = 'Kirim email konfirmasi setelah berlangganan';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_06933067aafd48425d67bcb01bba5cb6'] = 'Update';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_6e659c47c94d1e1dc7121859f43fb2b0'] = 'Alamat email tdk valid';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_9e6df6e72be5be5b8ff962ee3406907e'] = 'Alamat email tdk terdaftar';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_29003419c075963848f1907174bdc224'] = 'Kesalahan sewaktu  membatalkan berlangganan';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_f7dc297e2a139ab4f5a771825b46df43'] = 'batalkan berlangganan berhasil';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_8dc3b88902df97bb96930282e56ed381'] = 'Alamat email sudah pernah terdaftar';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b7d9eb38dd2e375648ab08e224e22e43'] = 'Error sewaktu berlangganan';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ed3cd7b3cc134222fa70602921ec27e1'] = 'Berlangganan berhasil';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_efabbb0eb84d3c8e487a72e879953673'] = 'Konfirmasi Newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'Newsletter';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_416f61a2ce16586f8289d41117a2554e'] = 'email anda';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_b26917587d98330d93f87808fc9d7267'] = 'Berlangganan';
$_MODULE['<{blocknewsletter}prestashop>blocknewsletter_4182c8f19d40c7ca236a5f4f83faeb6b'] = 'Batalkan berlangganan';
